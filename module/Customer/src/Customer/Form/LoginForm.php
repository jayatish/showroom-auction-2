<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Customer\Form;
use Zend\Form\Form;

class LoginForm extends Form{
    public function __construct($name = null) {
        parent::__construct('loginform');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'type'=>'text',
            ),
            'option'=>array(
                'label'=>'Email',
            ),
        ));
        $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'type'=>'password',
                'value'=>'',
            ),
            'option'=>array(
                'label'=>'Username',
            ),
        ));
        $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
                'value'=>'Login',
                'id'=>'submitbutton',
            ),
        ));
    }
}
