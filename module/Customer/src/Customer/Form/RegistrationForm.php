<?php

namespace Customer\Form;

use Zend\Form\Form;

class RegistrationForm extends Form {

    public function __construct($name = null) {
        parent::__construct('registration');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'dealer_type',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'Dealer Type',
            ),*/
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
            ),
             /*'options' => array(
                'label' => 'Username',
            ),*/
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
            ),
            /*'options' => array(
                'label' => 'E-mail',
            ),*/
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
            ),
            /*'options' => array(
                'label' => 'Password',
            ),*/
        ));

        $this->add(array(
            'name' => 'password_confirm',
            'attributes' => array(
                'type' => 'password',
            ),
            /*'options' => array(
                'label' => 'Confirm Password',
            ),*/
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'submitbutton',
            ),
        ));
        
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'User Name',
            ),*/
        ));
        
        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'State',
            ),*/
        ));
        
        $this->add(array(
            'name' => 'position_in_company',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'Position in Company',
            ),*/
        ));

        $this->add(array(
            'name' => 'website',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'Website',
            ),*/
        ));

        $this->add(array(
            'name' => 'company_name',
            'attributes' => array(
                'type' => 'text',
            ),
           /* 'options' => array(
                'label' => 'Company Name',
            ),*/
        ));

        $this->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
            ),
           /* 'options' => array(
                'label' => 'Mobile',
            ),*/
        ));

        $this->add(array(
            'name' => 'telephone',
            'attributes' => array(
                'type' => 'text',
            ),
            /*'options' => array(
                'label' => 'Telephone',
            ),*/
        ));


    }

}
