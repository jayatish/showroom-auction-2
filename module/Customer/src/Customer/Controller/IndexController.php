<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Customer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\TableGateway\TableGateway;
use Customer\Form\LoginForm;
use Customer\Model\Loginfilter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\Result;

class IndexController extends AbstractActionController {

    private $usersTable;

    public function indexAction() {
//        return new ViewModel(array('recordset' => $this->getUsersTable()->select()));
        if ($user = $this->identity()) {
            return $this->redirect()->toRoute('customer/default', array('controller' => 'index', 'action' => 'welcome'));
        } else {
//            return $this->redirect()->toRoute('customer/default', array('controller' => 'index', 'action' => 'login'));
            $form = new LoginForm();
            $messages = null;
            $request = $this->getRequest();
            if ($request->isPost()) {
                $formfilter = new Loginfilter();
                $form->setInputFilter($formfilter->getInputFilter());
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $data = $form->getData();
                    $sl = $this->getServiceLocator();
                    $dbAdapter = $sl->get('Zend\Db\Adapter\Adapter');

                    $config = $this->getServiceLocator()->get('Config');

                    $authAdapter = new AuthAdapter($dbAdapter);
                    $authAdapter
                            ->setTableName('customer')
                            ->setIdentityColumn('email')
                            ->setCredentialColumn('password')
                    ;

                    $email = $data['email'];
                    $pass = $data['password'];
                    $authAdapter->setIdentity($email);
                    $authAdapter->setCredential(md5($pass));

                    $auth = new AuthenticationService();
                    $result = $auth->authenticate($authAdapter);

                    switch ($result->getCode()) {
                        case Result::FAILURE_IDENTITY_NOT_FOUND:
                            break;

                        case Result::FAILURE_CREDENTIAL_INVALID:
                            break;

                        case Result::SUCCESS:
                            $storage = $auth->getStorage();
                            $storage->write($authAdapter->getResultRowObject(
                                            null, 'password'
                            ));
                            $sessionManager = new \Zend\Session\SessionManager();
                            $sessionManager->rememberMe(46498798);
                            break;
                        default:
                            break;
                    }
                    foreach ($result->getMessages() as $message) {
                        $messages .= "$message\n";
                    }

                    if ($result->getCode() == Result::SUCCESS) {
                        return $this->redirect()->toRoute('customer/default', array('controller' => 'index', 'action' => 'welcome'));
                    }
                }
            }
            return new ViewModel(array('form' => $form, 'messages' => $messages));
        }
        return new ViewModel();
    }

    public function welcomeAction() {
        if (!$this->identity()) {
            return $this->redirect()->toRoute('application/default', array('controller' => 'index', 'action' => 'index'));
        }
        return new ViewModel((array) $this->identity());
    }

    public function loginAction() {
        $form = new LoginForm();
        $messages = null;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formfilter = new Loginfilter();
            $form->setInputFilter($formfilter->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                $sl = $this->getServiceLocator();
                $dbAdapter = $sl->get('Zend\Db\Adapter\Adapter');

                $config = $this->getServiceLocator()->get('Config');

                $authAdapter = new AuthAdapter($dbAdapter);
                $authAdapter
                        ->setTableName('customer')
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password')
                ;

                $email = $data['email'];
                $pass = $data['password'];
                $authAdapter->setIdentity($email);
                $authAdapter->setCredential(md5($pass));

                $auth = new AuthenticationService();
                $result = $auth->authenticate($authAdapter);

                switch ($result->getCode()) {
                    case Result::FAILURE_IDENTITY_NOT_FOUND:
                        break;

                    case Result::FAILURE_CREDENTIAL_INVALID:
                        break;

                    case Result::SUCCESS:
                        $storage = $auth->getStorage();
                        $storage->write($authAdapter->getResultRowObject(
                                        null, 'password'
                        ));
                        $sessionManager = new \Zend\Session\SessionManager();
                        $sessionManager->rememberMe(46498798);
                        break;
                    default:
                        break;
                }
                foreach ($result->getMessages() as $message) {
                    $messages .= "$message\n";
                }

                if ($result->getCode() == Result::SUCCESS) {
                    return $this->redirect()->toRoute('customer/default', array('controller' => 'index', 'action' => 'welcome'));
                }
            }
        }
        return new ViewModel(array('form' => $form, 'messages' => $messages));
    }

    public function logoutAction() {
        $auth = new AuthenticationService();

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
        }

        $auth->clearIdentity();
        $sessionManager = new \Zend\Session\SessionManager();
        $sessionManager->forgetMe();

        return $this->redirect()->toRoute('application/default', array('controller' => 'index', 'action' => 'index'));
    }

    public function getUsersTable() {
        if (!$this->usersTable) {
            $this->usersTable = new TableGateway(
                    'customer', $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
            );
        }
        return $this->usersTable;
    }
}
